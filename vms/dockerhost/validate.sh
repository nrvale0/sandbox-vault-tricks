#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -ue

# the internet says this is better than 'set -e'
function onerr {
    echo 'Cleaning up after error...'
    exit -1
}
trap onerr ERR

echo "Validating the Docker host..."

pushd `pwd` > /dev/null 2>&1
cd /vagrant > /dev/null 2>&1 || cd "$(dirname $0)/../../"

(set -x; \
    inspec exec vms/dockerhost/validate.d/inspec)

popd > /dev/null 2>&1
