vault_node = %x(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq --filter='name=vault-enterprise0')).split

consul_node = %x(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq --filter='name=consul-enterprise0')).split

describe http("http://${consul_node}:8500") do
  its('status') { should cmd 200 }
end

describe http("http://#{vault_node}:8200/v1/sys/leader") do
  its('status') { should cmp 200 }
end
