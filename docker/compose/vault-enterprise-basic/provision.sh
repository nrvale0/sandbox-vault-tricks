#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -ue

# the internet says this is better than 'set -e'
function onerr {
    echo 'Cleaning up after error...'
    exit -1
}
trap onerr ERR

echo "Provisioning Vault Enterprise via Docker Compose..."

pushd `pwd` > /dev/null 2>&1
cd /vagrant > /dev/null 2>&1 || cd "$(dirname $0)/../../"

(set -x ; \
 cd docker/compose/vault-enterprise-basic && \
     docker-compose up --build -d)

popd > /dev/null 2>&1
