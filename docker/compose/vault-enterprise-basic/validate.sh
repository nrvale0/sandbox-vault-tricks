#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -ue

# the internet says this is better than 'set -e'
function onerr {
    echo 'Cleaning up after error...'
    exit -1
}
trap onerr ERR

echo "Validating Vault Enterprise via InSpec..."

pushd `pwd` > /dev/null 2>&1
cd /vagrant > /dev/null 2>&1 || cd "$(dirname $0)/../../"

function consul_enterprise0_ip() {
    docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'
}

echo "Sleeping for a few seconds to give Consul time to settle..."
sleep 7

(set -x && \
     cd docker/compose/vault-enterprise-basic/ && \
     RUBYOPT=-W0 inspec exec validate.d/inspec/containers.rb && \
     RUBYOPT=-W0 inspec exec validate.d/inspec/consul.rb && \
     RUBYOPT=-W0 inspec exec validate.d/inspec/vault.rb)

popd > /dev/null 2>&1
