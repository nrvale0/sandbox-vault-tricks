#!/bin/bash

pushd `pwd`
cd "$(dirname $0)/../"

function onerr {
    echo "Cleaning up after failure..."
    popd > /dev/null 2>&1 || true
    exit -1
}
trap onerr ERR

set -e
if [ "${DEBUG}" ]; then
    set -x
fi

set -u

: ${VAULT_ADDR:="http://127.0.0.1:8200"}
: ${POSTGRES_ACCOUNT_DEFAULT_TTL:="10m"}

clear

echo "Mounting Vault database backend..."
vault mount database

echo "Writing the Postgres database backend config..."
vault write database/config/postgresql plugin_name=postgresql-database-plugin allowed_roles="readonly,writeall" connection_url="postgresql://postgres:postgres@postgres:5432/testdb?sslmode=disable"

echo "Creating a read-only policy which has read-only access to all tables in public schema on 'testdb'..."
vault write database/roles/readonly \
      db_name=postgresql \
      creation_statements="CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}'; GRANT SELECT ON ALL TABLES IN SCHEMA public TO \"{{name}}\";" \
      default_ttl="${POSTGRES_ACCOUNT_DEFAULT_TTL}" \
      max_ttl="1h"

echo "Creating a writeall role which can write to all tables in public schema on 'testdb'..."
vault write database/roles/writeall \
      db_name=postgresql \
      creation_statements="CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}'; GRANT ALL ON ALL TABLES IN SCHEMA public TO \"{{name}}\";" \
      default_ttl="${POSTGRES_ACCOUNT_DEFAULT_TTL}" \
      max_ttl="1h"

echo "Read the read-only account credentials..."
vault read database/creds/readonly

echo "Read the write-all account credentials..."
vault read database/creds/writeall
