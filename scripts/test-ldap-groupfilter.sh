#!/bin/bash

: ${LDAP_USER:="cn=vagrant,cn=Users,dc=example,dc=com"}

set -eux


ldapsearch -h localhost -p 3890 -P 3 -b dc=example,dc=com -D cn=Administrator,cn=Users,dc=example,dc=com -w HeyH0Password -s sub "(&(objectclass=group)(member:1.2.840.113556.1.4.1941:=${LDAP_USER}))"
