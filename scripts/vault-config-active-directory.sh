#!/bin/bash


: ${LDAP_URL:="ldap://localhost:3890"}
: ${VAULT_ADDR:="http://localhost:8200"}
: ${LDAP_URL:="http://localhost:389"}
: ${USER_DN:="cn=Users,dc=example,dc=com"}
: ${GROUP_DN:="cn=Users,dc=example,dc=com"}
: ${GROUP_FILTER:="(&(objectclass=group)(cn=pg-users)(member:1.2.840.113556.1.4.1941:={{.UserDN}}))"}
: ${GROUP_ATTR:="cn"}
: ${BIND_DN:="cn=Administrator,cn=Users,dc=example,dc=com"}
: ${BIND_PASSWORD:="HeyH0Password"}


[ -z "${DEBUG}" ] || set -x
set -u

# the internet says this is better than 'set -e'
function onerr {
    echo 'Cleaning up after error...'
    exit -1     
}
trap onerr ERR


function main() {

    export VAULT_ADDR="${VAULT_ADDR}"
    
    echo "Checking for running Vault..."
    vault status

    echo "Enabling LDAP auth backend..."
    vault auth -methods | grep ldap 2>&1 > /dev/null
    if [ "0" == "$?" ]; then
	vault auth-disable ldap
	sleep 5
    fi
    vault auth-enable ldap

    echo "Configuring LDAP auth backend..."
    vault write auth/ldap/config \
	  url="${LDAP_URL}" \
	  userdn="${USER_DN}" \
	  groupdn="${GROUP_DN}" \
	  groupfilter="${GROUP_FILTER}" \
	  groupattr="${GROUP_ATTR}" \
	  insecure_tls=true \
	  starttls=false \
	  binddn="${BIND_DN}" \
	  bindpass="${BIND_PASSWORD}"
}

main


